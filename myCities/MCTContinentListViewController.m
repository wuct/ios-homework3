//
//  MCTContinentListViewController.m
//  myCities
//
//  Created by WUCT on 12/10/21.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import "MCTContinentListViewController.h"
#import "MCTCityListViewController.h"
#import "MCTContinentListCell.h"
#import "MCTDataSource.h"

@interface MCTContinentListViewController ()

@end

@implementation MCTContinentListViewController


#pragma mark - Table view data source

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[MCTDataSource sharedDataSource] arrayWithContinents] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Continent";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    // Fetch continent data by index path from city list
    NSUInteger row = indexPath.row;
    NSString *continent = [[MCTDataSource sharedDataSource] arrayWithContinents][row];

    // Assign values to views in cell
    MCTContinentListCell *continentCell = (MCTContinentListCell *)cell;
    continentCell.continentName.text = continent;

    return cell;
}

#pragma mark - Storyboard

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UITableViewCell *cell = (UITableViewCell *)sender;
    
    if ([segue.identifier isEqualToString:@"ShowCityList"]) {
        // Fetch data by index path from data source
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        NSUInteger row = indexPath.row;
        NSString *continent = [[MCTDataSource sharedDataSource] arrayWithContinents][row];
        
        // Feed data to the destination of the segue
        MCTCityListViewController *detailPage = segue.destinationViewController;
        detailPage.continent = continent;
    }
}

@end
