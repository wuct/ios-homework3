//
//  main.m
//  myCities
//
//  Created by 吳敬庭 on 12/10/21.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MCTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MCTAppDelegate class]));
    }
}
