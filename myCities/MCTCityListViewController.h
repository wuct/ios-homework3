//
//  MCTCityListViewController.h
//  myCities
//
//  Created by 吳敬庭 on 12/10/22.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MCTCityListViewController : UITableViewController

@property (strong, nonatomic) NSString *continent;

@end
