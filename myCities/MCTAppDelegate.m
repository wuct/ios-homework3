//
//  MCTAppDelegate.m
//  myCities
//
//  Created by 吳敬庭 on 12/10/21.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import "MCTAppDelegate.h"
#import "MCTDataSource.h"
@implementation MCTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
//    NSLog(@"yes, finish launching app!");
//    NSLog(@"Let try whether data source was loaded!");
//    NSLog([[[MCTDataSource sharedDataSource] arrayWithContinents] description]);
    
    return YES;
}
							
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    [[MCTDataSource sharedDataSource] cleanCache];
}

@end
