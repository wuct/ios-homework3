//
//  MCTAppDelegate.h
//  myCities
//
//  Created by 吳敬庭 on 12/10/21.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
