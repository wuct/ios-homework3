//
//  MCTDataSource.m
//  myCities
//
//  Created by WUCT on 12/10/21.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import "MCTDataSource.h"
//Cache Keys
static NSString *MCTDataSourceCacheKeyContinents = @"MCTDataSource.Cache.Continents";
static NSString *MCTDataSourceCacheKeyCountries = @"MCTDataSource.Cache.%@.Countries";
static NSString *MCTDataSourceCacheKeyCities = @"MCTDataSource.Cache.%@.%@.Cities";

//Dicitionary Keys
NSString * const MCTDataSourceDictKeyCity = @"City";
NSString * const MCTDataSourceDictKeyRegion = @"Region";
NSString * const MCTDataSourceDictKeyContinent = @"Continent";
NSString * const MCTDataSourceDictKeyCountry = @"Country";
NSString * const MCTDataSourceDictKeyLocal = @"Local";
NSString * const MCTDataSourceDictKeyImage = @"Image";

@implementation MCTDataSource

#pragma mark -
#pragma mark Object Lifecycle
//這段看不懂
+ (MCTDataSource *)sharedDataSource{
    static dispatch_once_t once;
    static MCTDataSource *sharedDataSource;
    dispatch_once(&once, ^ {
        sharedDataSource = [[self alloc] init];
    });
    return sharedDataSource;
}

-(id)init {
    if (self=[super init]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"citylist" ofType:@"plist"];
        cityList = [NSArray arrayWithContentsOfFile:path];
        
        cache = [[NSCache alloc] init];
    }
    return self;
}

#pragma mark-
#pragma mark Interfaces

-(void)refresh{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"citylist" ofType:@"plist"];
    cityList = [NSArray arrayWithContentsOfFile:path];
    
    [self cleanCache];
}

-(void)cleanCache{
    [cache removeAllObjects];
}

-(NSArray *)arrayWithContinents{
    NSArray *continents = [cache objectForKey:MCTDataSourceCacheKeyContinents];
    
    if(!continents){
        //Save continents into a set (remove duplicate result)
        NSMutableSet *continentsSet = [NSMutableSet set];
        for (NSDictionary *city in cityList)
            [continentsSet addObject:city[MCTDataSourceDictKeyContinent]];
        
        // Convert set to array and sort the array.
        continents = [[continentsSet allObjects] sortedArrayUsingComparator:
                     ^NSComparisonResult(id obj1, id obj2) {
                         return [obj1 compare:obj2];
                     }];
        
        // Save the result into cache
        [cache setObject:continents forKey:MCTDataSourceCacheKeyContinents];
    }
    
    return continents;
}

-(NSArray *)arrayWithCountriesInContinents:(NSString *)continent{
    NSString *cacheKey = [NSString stringWithFormat:MCTDataSourceCacheKeyCountries, continent];
    NSArray *resultCountries = [cache objectForKey:cacheKey];
    
    if (!resultCountries) {
        // Filter array
        resultCountries = [cityList filteredArrayUsingPredicate:
                          [NSPredicate predicateWithBlock:
                           ^BOOL(id evaluatedObject, NSDictionary *bindings) {
                               NSDictionary *city = (NSDictionary *)evaluatedObject;
                               return [city[MCTDataSourceDictKeyContinent] isEqualToString:continent];
                           }]];
        //Save countries into a set (remove duplicate result)
        NSMutableSet *countriesSet = [NSMutableSet set];
        for (NSDictionary *city in resultCountries) {
            [countriesSet addObject:city[MCTDataSourceDictKeyCountry]];
        }

        // Convert set to array and sort the array.
        resultCountries = [[countriesSet allObjects] sortedArrayUsingComparator:
                           ^NSComparisonResult(id obj1, id obj2) {
                               return [obj1 compare:obj2];
                           }];
        
        // Save the result into cache
        [cache setObject:resultCountries forKey:MCTDataSourceCacheKeyCountries];
    }
    
    return resultCountries;
}
 
-(NSArray *)arrayWithCitiesInContinents:(NSString *)continent inCountry:(NSString *)country{
    NSString *cacheKey = [NSString stringWithFormat:MCTDataSourceCacheKeyCities,continent,country];
    NSArray *resultCities = [cache objectForKey:cacheKey];
    
    if (!resultCities) {
        // Filter array by continents and countries
        resultCities = [cityList filteredArrayUsingPredicate:
                           [NSPredicate predicateWithBlock:
                            ^BOOL(id evaluatedObject, NSDictionary *bindings) {
                                NSDictionary *city = (NSDictionary *)evaluatedObject;
                                return [city[MCTDataSourceDictKeyContinent] isEqualToString:continent]&[city[MCTDataSourceDictKeyCountry] isEqualToString:country];
                            }]];
        // Sort array
        resultCities = [resultCities sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [obj1[MCTDataSourceDictKeyCity] compare:obj2[MCTDataSourceDictKeyCity]];
        }];
        // Save the result into cache
        [cache setObject:resultCities forKey:MCTDataSourceCacheKeyCities];
    }
    
    return resultCities;
}

@end
