//
//  MCTContinentListCell.h
//  myCities
//
//  Created by WUCT on 12/10/22.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCTContinentListCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *continentName;

@end
