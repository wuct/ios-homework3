//
//  MCTDataSource.h
//  myCities
//
//  Created by WUCT on 12/10/21.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const MCTDataSourceDictKeyCity;
extern NSString * const MCTDataSourceDictKeyRegion;
extern NSString * const MCTDataSourceDictKeyContinent;
extern NSString * const MCTDataSourceDictKeyCountry;
extern NSString * const MCTDataSourceDictKeyLocal;
extern NSString * const MCTDataSourceDictKeyImage;

@interface MCTDataSource : NSObject{
    //main data pool
    NSArray *cityList;
    //cache data pool
    NSCache *cache;
}

+ (MCTDataSource *)sharedDataSource;

-(void)refresh;
-(void)cleanCache;
-(NSArray *)arrayWithContinents;
-(NSArray *)arrayWithCountriesInContinents:(NSString *)continent;
-(NSArray *)arrayWithCitiesInContinents:(NSString *)continent inCountry:(NSString *)country;

@end
