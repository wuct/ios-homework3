//
//  MCTDetailViewController.m
//  myCities
//
//  Created by 吳敬庭 on 12/10/22.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import "MCTDetailViewController.h"
#import "MCTDataSource.h"

@interface MCTDetailViewController ()

@end

@implementation MCTDetailViewController

- (void)viewDidLoad{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.cityName.text=self.city[MCTDataSourceDictKeyCity];
    self.regionName.text=self.city[MCTDataSourceDictKeyRegion];
    self.localName.text=self.city[MCTDataSourceDictKeyLocal];
    self.imageView.image=[UIImage imageNamed:self.city[MCTDataSourceDictKeyImage]];
}

@end
