//
//  MCTCityListViewController.m
//  myCities
//
//  Created by 吳敬庭 on 12/10/22.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import "MCTCityListViewController.h"
#import "MCTCityListCell.h"
#import "MCTDetailViewController.h"
#import "MCTDataSource.h"

@interface MCTCityListViewController ()

@end

@implementation MCTCityListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[[MCTDataSource sharedDataSource] arrayWithCountriesInContinents:self.continent] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *country = [[MCTDataSource sharedDataSource] arrayWithCountriesInContinents:self.continent][section];
    return [[[MCTDataSource sharedDataSource] arrayWithCitiesInContinents:self.continent inCountry:country] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    // Fetch city data by index path from city list
    NSUInteger section = indexPath.section;
    NSUInteger row = indexPath.row;
    NSString *country = [[MCTDataSource sharedDataSource] arrayWithCountriesInContinents:self.continent][section];
    NSDictionary *city = [[MCTDataSource sharedDataSource] arrayWithCitiesInContinents:self.continent inCountry:country][row];
    
    // Assign values to views in cell
     MCTCityListCell *cityCell = (MCTCityListCell *)cell;
    cityCell.cityName.text = city[MCTDataSourceDictKeyCity];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[MCTDataSource sharedDataSource] arrayWithCountriesInContinents:self.continent][section];
}


#pragma mark - Storyboard

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UITableViewCell *cell = (UITableViewCell *)sender;
    
    if ([segue.identifier isEqualToString:@"ShowDetail"]) {
        // Fetch city data by index path from city list
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        NSUInteger section = indexPath.section;
        NSUInteger row = indexPath.row;
        NSString *country = [[MCTDataSource sharedDataSource] arrayWithCountriesInContinents:self.continent][section];
        NSDictionary *city = [[MCTDataSource sharedDataSource] arrayWithCitiesInContinents:self.continent inCountry:country][row];
        
        // Feed data to the destination of the segue
        MCTDetailViewController *detailPage = segue.destinationViewController;
        detailPage.city = city;
    }
}

@end
