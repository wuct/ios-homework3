//
//  MCTDetailViewController.h
//  myCities
//
//  Created by 吳敬庭 on 12/10/22.
//  Copyright (c) 2012年 Wuct. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCTDetailViewController : UIViewController

@property (strong, nonatomic) NSDictionary *city;
@property (strong, nonatomic) IBOutlet UILabel *cityName;
@property (strong, nonatomic) IBOutlet UILabel *regionName;
@property (strong, nonatomic) IBOutlet UILabel *localName;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end
